<?php

namespace Drupal\actions_queue;

/**
 * Interface for actions_queue_jobs plugins.
 */
interface ActionsQueueJobsInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label() :string;

  /**
   * The actions this plugin supports.
   *
   * The is a key=>value array where the value corresponds
   * to the callback executed.
   *
   * @code
   * $actions = [
   *    Queue::ACTIONS_QUEUE_ENTITY_UPDATE => 'entityUpdateJob',
   *    Queue::ACTIONS_QUEUE_ENTITY_INSERT => 'entityInsertJob',
   * ];
   * @endcode
   *
   * @return array
   *   An array of Action ids that this plugin supports.
   */
  public function actions() :array;

  /**
   * This plugin is active or inactive.
   *
   * @return bool
   *   Return true for active plugin, false for inactive.
   */
  public function isActive() :bool;

  /**
   * This is a valid item to be executed.
   *
   * @param array $data
   *   The input data.
   *
   * @return bool
   *   Return FALSE if you do not want to execute for the current item.
   */
  public function isValid(array $data) :bool;

}
