<?php

namespace Drupal\actions_queue;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;

/**
 * Implements a logging functionality.
 */
class Log {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;
  /**
   * The Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private TimeInterface $time;

  /**
   * Constructs a Log object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   */
  public function __construct(Connection $connection, TimeInterface $time) {
    $this->connection = $connection;
    $this->time = $time;
  }

  /**
   * Inserts data to the db.
   *
   * @param string $event
   *   Some txt descrption for this log event.
   * @param array $data
   *   The input data array.
   * @param bool $error
   *   Whether this entry indicates an error or not.
   * @param string $details
   *   The error description.
   *
   * @throws \JsonException
   */
  public function log(string $event, array $data, bool $error = FALSE, string $details = '') :void {

    $this->connection->insert('actions_queue_log')
      ->fields([
        'entity_id' => $data['entity_id'],
        'entity_type' => $data['entity_type'],
        'event' => $event,
        'data' => json_encode($data),
        'is_error' => (int) $error,
        'details' => $details,
        'created' => $this->time->getRequestTime(),
      ])->execute();
  }

}
