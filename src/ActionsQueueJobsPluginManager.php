<?php

namespace Drupal\actions_queue;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * ActionsQueueJobs plugin manager.
 */
class ActionsQueueJobsPluginManager extends DefaultPluginManager {

  /**
   * Constructs ActionsQueueJobsPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/ActionsQueueJobs',
      $namespaces,
      $module_handler,
      'Drupal\actions_queue\ActionsQueueJobsInterface',
      'Drupal\actions_queue\Annotation\ActionsQueueJobs'
    );
    $this->alterInfo('actions_queue_jobs_info');
    $this->setCacheBackend($cache_backend, 'actions_queue_jobs_plugins');
  }

}
