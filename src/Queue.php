<?php

namespace Drupal\actions_queue;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Queue\QueueFactory;

/**
 * Receives the hook events and adds them to the queue.
 */
class Queue {

  // Define the Actions constants. These constants correspond to the
  // hooks implemented in the .module file.
  public const ACTIONS_QUEUE_ENTITY_INSERT = 'entity_insert';
  public const ACTIONS_QUEUE_ENTITY_UPDATE = 'entity_update';
  public const ACTIONS_QUEUE_ENTITY_DELETE = 'entity_delete';
  public const ACTIONS_QUEUE_TRANSLATION_DELETE = 'translation_delete';

  /**
   * The queue factory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  private QueueFactory $queueFactory;
  /**
   * The plugin Manager.
   *
   * @var \Drupal\actions_queue\ActionsQueueJobsPluginManager
   */
  private ActionsQueueJobsPluginManager $pluginManager;
  /**
   * The custom log service.
   *
   * @var \Drupal\actions_queue\Log
   */
  private Log $logger;

  /**
   * Queue constructor.
   *
   * @param \Drupal\actions_queue\Log $log
   *   The log service.
   * @param \Drupal\actions_queue\ActionsQueueJobsPluginManager $pluginManager
   *   The plugin manager.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The queue factory service.
   */
  public function __construct(Log $log, ActionsQueueJobsPluginManager $pluginManager, QueueFactory $queueFactory) {
    $this->logger = $log;
    $this->pluginManager = $pluginManager;
    $this->queueFactory = $queueFactory;
  }

  /**
   * Adds the item to the Queue (or allows for immediate execution).
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to be added to the queue.
   * @param string $action_type
   *   The action type for the queue item.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function addToQueue(EntityInterface $entity, string $action_type): void {

    $data = [
      'entity_type' => $entity->getEntityTypeId(),
      'entity_id' => $entity->id(),
      'action_type' => $action_type,
    ];
    $queue_name = 'actions_queue_jobs_queue';
    $queue = $this->queueFactory->get($queue_name);

    // Get all plugins.
    $plugin_definitions = $this->pluginManager->getDefinitions();
    foreach ($plugin_definitions as $plugin_definition) {

      /** @var \Drupal\actions_queue\ActionsQueueJobsPluginBase $plugin */
      $plugin = $this->pluginManager->createInstance($plugin_definition['id']);
      $actions = $plugin->actions();

      $data['plugin_id'] = $plugin_definition['id'];
      $data['langcode'] = $entity->language()->getId();

      // Check that the plugin implements the action and that it is active
      // and valid.
      if (!array_key_exists($action_type, $actions) || !$plugin->isActive() || !$plugin->isValid($data)) {
        return;
      }

      // For more complex cases you may want to implement your own queues in
      // your custom module. Here we are checking if this is such case.
      if ($plugin->skipActionsQueueQueue()) {
        $this->executeQueueItem($data);
      }
      else {
        $this->logger->log('Added to queue', $data);
        $queue->createItem($data);
      }
    }
  }

  /**
   * Executes a queue item.
   *
   * @param array $data
   *   The data array.
   *
   * @throws \Exception
   */
  public function executeQueueItem(array $data) {
    /** @var \Drupal\actions_queue\ActionsQueueJobsPluginBase $plugin */
    $plugin = $this->pluginManager->createInstance($data['plugin_id']);
    $actions = $plugin->actions();

    if (array_key_exists($data['action_type'], $actions)) {
      // Find the job name and execute it.
      $job = $plugin->actions()[$data['action_type']];
      $plugin->{$job}($data);
    }
  }

}
