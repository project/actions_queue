<?php

namespace Drupal\actions_queue\Commands;

use Drupal\actions_queue\Queue;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\Messenger;
use Drush\Commands\DrushCommands;

/**
 * Drush commandfile for actions_queue.
 */
class ActionsQueueCommands extends DrushCommands {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * The actions queue service.
   *
   * @var \Drupal\actions_queue\Queue
   */
  private Queue $queue;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  private $messenger;

  /**
   * The constructor for the drush command file.
   *
   * @param \Drupal\actions_queue\Queue $queue
   *   The actions queue service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger service.
   */
  public function __construct(
    Queue $queue,
    EntityTypeManagerInterface $entityTypeManager,
    Messenger $messenger
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->queue = $queue;
    $this->messenger = $messenger;
  }

  /**
   * Drush command definition.
   *
   * @param array $options
   *   An associative array of options.
   *
   * @option string entity_type
   *   The entity type
   * @option string entity_id
   *   A comma seperated list of entity ids. Skip this and include all ids.
   * @usage actions_queue:add --entity_type=node --entity_id=1,2,3
   *   Usage description
   * @command actions_queue:add
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function add(array $options = [
    'entity_type' => 'node',
    'entity_id' => '',
  ]) {

    $storage = $this->entityTypeManager->getStorage($options['entity_type']);

    if (!empty($options['entity_id'])) {
      $entity_ids = explode(',', $options['entity_id']);
    }
    else {
      $entity_ids = $storage->getQuery()->execute();
    }

    foreach ($entity_ids as $id) {
      /** @var \Drupal\node\Entity\Node $entity */
      $entity = $storage->load($id);
      if (!$entity instanceof EntityInterface) {
        continue;
      }
      $langs = $entity->getTranslationLanguages();
      foreach ($langs as $language) {
        $n_trans = $entity->getTranslation($language->getId());
        $this->messenger->addStatus('Adding id: ' . $id . ' ' . $language->getId());
        $this->queue->addToQueue($n_trans, Queue::ACTIONS_QUEUE_ENTITY_INSERT);
      }
    }
  }

}
