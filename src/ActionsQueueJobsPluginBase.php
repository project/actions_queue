<?php

namespace Drupal\actions_queue;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for actions_queue_jobs plugins.
 */
abstract class ActionsQueueJobsPluginBase extends PluginBase implements ActionsQueueJobsInterface {

  /**
   * {@inheritdoc}
   */
  public function label() :string {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * Return TRUE if immediate processing is required.
   *
   * @return bool
   *   Whether to skip the queue and execute immediately or not.
   */
  public function skipActionsQueueQueue(): bool {
    return FALSE;
  }

}
