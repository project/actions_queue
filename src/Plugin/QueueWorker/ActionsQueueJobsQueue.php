<?php

namespace Drupal\actions_queue\Plugin\QueueWorker;

use Drupal\actions_queue\Queue;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines 'actions_queue_queue' queue worker.
 *
 * @QueueWorker(
 *   id = "actions_queue_jobs_queue",
 *   title = @Translation("ActionsQueueJobsQueue"),
 *   cron = {"time" = 60}
 * )
 */
final class ActionsQueueJobsQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The Actions Queue service.
   *
   * @var \Drupal\actions_queue\Queue
   */
  private Queue $actionsQueue;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Queue $actionsQueue) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->actionsQueue = $actionsQueue;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('actions_queue.queue'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $this->actionsQueue->executeQueueItem($data);
  }

}
