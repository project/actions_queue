<?php

namespace Drupal\actions_queue\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ResultRow;

/**
 * Field handler for json field.
 *
 * Retrieves specific value from the json based on configuration.
 *
 * @ViewsField("views_json_field")
 */
class ViewsJsonField extends FieldPluginBase {

  /**
   * Render.
   */
  public function render(ResultRow $values) {
    $key = $this->field_alias;

    if (!isset($values->$key)) {
      return '';
    }

    $json_key = $this->options['key'];
    $decoded = json_decode($values->$key, TRUE);
    if (isset($decoded[$json_key])) {
      return $decoded[$json_key];
    }
    return '';
  }

  /**
   * Option definition.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['key'] = ['default' => ''];
    $options['trusted_html'] = ['default' => 0];
    return $options;
  }

  /**
   * Options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['key'] = [
      '#title' => $this->t('Key Chooser'),
      '#description' => $this->t('choose a key'),
      '#type' => 'textfield',
      '#default_value' => $this->options['key'],
      '#required' => TRUE,
    ];
  }

}
