# Actions Queue

This module provides the ability to execute hook events asynchronously via
queues. It reacts to the hook_entity create/update/delete and adds a job
to a queue. Additionally, the module implements a table for logging
relevant events.

Here is a sample job from the queue:

```
Array
(
    [entity_type] => node
    [entity_id] => 1287
    [action_type] => entity_insert
    [langcode] => de
)
```

This module currently implements the following hooks:

* hook_entity_insert().
* hook_entity_update().
* hook_entity_entity_delete().
* hook_entity_translation_delete().

Developers who wish to use this module must create a `ActionsQueueJobs` plugin.
Check the example module for an implementation of the plugin.
