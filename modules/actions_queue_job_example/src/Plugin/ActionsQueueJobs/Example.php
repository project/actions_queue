<?php

namespace Drupal\actions_queue_job_example\Plugin\ActionsQueueJobs;

use Drupal\actions_queue\Log;
use Drupal\actions_queue\Queue;
use Drupal\actions_queue\ActionsQueueJobsPluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the actions_queue_jobs.
 *
 * @ActionsQueueJobs(
 *   id = "actions_queue_job_example",
 *   label = @Translation("Job example"),
 *   description = @Translation("An example plugin for the Actions Queue module.")
 * )
 */
final class Example extends ActionsQueueJobsPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The Log service.
   *
   * @var \Drupal\actions_queue\Log
   */
  private Log $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Log $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('actions_queue.log'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function actions() : array {
    return [
      Queue::ACTIONS_QUEUE_ENTITY_UPDATE => 'entityUpdate',
      Queue::ACTIONS_QUEUE_ENTITY_INSERT => 'entityInsert',
      Queue::ACTIONS_QUEUE_ENTITY_DELETE => 'entityDelete',
    ];
  }

  /**
   * The callback to execute, defined in the self::actions().
   *
   * @param array $data
   *   The data array.
   */
  public function entityUpdate(array $data): void {
    $this->logger->log('Example entity update.', $data);
  }

  /**
   * The callback to execute, defined in the self::actions().
   *
   * @param array $data
   *   The data array.
   */
  public function entityInsert(array $data): void {
    $this->logger->log('Example entity insert.', $data);
  }

  /**
   * The callback to execute, defined in the self::actions().
   *
   * @param array $data
   *   The data array.
   */
  public function entityDelete(array $data): void {
    $this->logger->log('Example entity delete.', $data);
  }

  /**
   * {@inheritdoc}
   */
  public function isValid(array $data): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isActive(): bool {
    return TRUE;
  }

}
