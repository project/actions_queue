<?php

/**
 * @file
 * Describes a database table to views module.
 */

/**
 * Implements hook_views_data().
 */
function actions_queue_views_data() {

  $data = [];

  $data['actions_queue_log'] = [];
  $data['actions_queue_log']['table'] = [];
  $data['actions_queue_log']['table']['group'] = t('Actions Queue Log');
  $data['actions_queue_log']['table']['provider'] = 'actions_queue';
  $data['actions_queue_log']['table']['base'] = [
    'field' => 'id',
    'title' => t('Actions Queue Log'),
    'help' => t('The Actions Queue Log table containing data relevant to actions executed.'),
    'weight' => -10,
  ];
  $data['actions_queue_log']['id'] = [
    'title' => t('ID'),
    'help' => t('The ID of this row.'),
    'field' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
  ];

  $data['actions_queue_log']['entity_id'] = [
    'title' => t('Entity ID'),
    'help' => t('The entity id this entry is for'),
    'field' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
  ];

  $data['actions_queue_log']['entity_type'] = [
    'title' => t('The entity type'),
    'help' => t('The entity type.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];

  $data['actions_queue_log']['event'] = [
    'title' => t('Event'),
    'help' => t('The event, usually a hook name.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];

  $data['actions_queue_log']['data'] = [
    'title' => t('Data'),
    'help' => t('Json encoded data.'),
    'field' => [
      'id' => 'views_json_field',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];

  $data['actions_queue_log']['is_error'] = [
    'title' => t('Is Error'),
    'help' => t('Is an error or not.'),
    'field' => [
      'id' => 'boolean',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'boolean',
      'label' => t('Is error'),
      'type' => 'yes-no',
      'use_equal' => TRUE,
    ],
  ];

  $data['actions_queue_log']['details'] = [
    'title' => t('Description'),
    'help' => t('Further description about this row.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];

  $data['actions_queue_log']['created'] = [
    'title' => t('Created'),
    'help' => t('Creation date.'),
    'field' => [
      'id' => 'date',
    ],
    'sort' => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
  ];

  return $data;

}
